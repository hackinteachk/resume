import Vue from 'vue'
import Router from 'vue-router'
import Landing from '../components/desktop/home/Landing'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Landing
    }
  ],
  mode: 'history'
})
